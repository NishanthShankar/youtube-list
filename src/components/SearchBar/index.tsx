import { useState } from 'react'
import { View, RView } from 'components/View'
import styles from './index.styles'
export interface ISearchBarProps {
  onSearch?: (a: string) => void
  query?: string
}

export interface IElement {
  target: {
    value: string
  }
}

export default function SearchBar (props: ISearchBarProps): JSX.Element | null {
  const [value, setValue] = useState(props.query)

  const onChange = (e: IElement): void => setValue(e.target.value)

  const onKeyDown = (element: { key: string }): void => {
    if (element.key === 'Enter') onSearch()
  }

  const onSearch = (): void => props?.onSearch?.(value ?? '')

  return (
    <RView style={styles.container}>
      <View flex style={styles.inputBar}>
        <input
          id='search'
          value={value}
          autoCapitalize='none'
          autoComplete='off'
          autoCorrect='off'
          onKeyDown={onKeyDown}
          name='search_query'
          type='text'
          spellCheck='false'
          placeholder='Search'
          aria-label='Search'
          onChange={onChange}
          style={styles.inputBox}
        />
      </View>
      <View
        id='search_icon'
        onClick={onSearch}
        center
        style={styles.iconContainer}
      >
        <img style={styles.icon} src='https://img.icons8.com/search' />
      </View>
    </RView>
  )
}
