export default {
  container: {
    margin: 12
  },
  inputBar: {
    height: 48,
    justifyContent: 'center',
    border: '1px solid hsl(0, 0%, 18.82%)'
  },
  inputBox: {
    outline: 'none',
    border: 'none',
    marginLeft: 4
  },
  iconContainer: {
    cursor: 'pointer',
    border: '1px solid hsl(0, 0%, 18.82%)',
    borderLeft: 0,
    width: 48
  },
  icon: {
    height: 32,
    width: 32
  }
}
