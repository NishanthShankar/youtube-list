import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'
import Component from './index'
import toJson from 'enzyme-to-json'
Enzyme.configure({ adapter: new Adapter() })

describe('<SearchBar />', () => {
  it('renders without props', () => {
    const tree = shallow(<Component />)
    expect(toJson(tree)).toMatchSnapshot()
  })

  it('renders with props', () => {
    const onSearch = jest.fn()
    const tree = shallow(<Component query='Programming' onSearch={onSearch} />)
    expect(toJson(tree)).toMatchSnapshot()
  })

  it('renders with query in place', () => {
    const text = 'Programming'
    const onSearch = jest.fn()
    const wrapper = shallow(<Component query={text} onSearch={onSearch} />)
    expect(wrapper.find('input').get(0).props.value).toEqual(text)
  })

  it('on search is called when search icon clicked', () => {
    const text = 'Programming'
    const onSearch = jest.fn()
    const wrapper = shallow(<Component query={text} onSearch={onSearch} />)
    wrapper.find('#search_icon').simulate('click')
    expect(onSearch).toBeCalled()
  })

  it('on search is called when enter is pressed', () => {
    const text = 'Programming'
    const onSearch = jest.fn()
    const wrapper = shallow(<Component query={text} onSearch={onSearch} />)
    const inputWrapper = wrapper.find('input')
    inputWrapper.simulate('keydown', { key: 'Enter' })
    expect(onSearch).toBeCalled()
  })
})
