import React, { useLayoutEffect, useRef } from 'react'
import Spinner from 'components/Spinner'

const styles = {
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: 100,
    width: 100
  }
}
export interface IAutoFetcherProps {
  hide?: boolean
  apiCall?: () => void
}

export default function AutoFetcher (
  props: IAutoFetcherProps
): JSX.Element | null {
  const ref = useRef(null)
  const observerCb = (entities: Array<{ isIntersecting: boolean }>): void => {
    if (entities?.[0]?.isIntersecting) props?.apiCall?.()
  }

  useLayoutEffect((): void => {
    if (ref.current == null) return
    const options = {
      root: null,
      rootMargin: '0px',
      threshold: 0.25
    }

    const observer = new IntersectionObserver(observerCb, options)
    observer.observe(ref.current)
  }, [ref.current])

  if (props.hide === true) return null
  return (
    <div ref={ref} style={styles.container}>
      <Spinner />
    </div>
  )
}
