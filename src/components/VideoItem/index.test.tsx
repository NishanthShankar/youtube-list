import Enzyme, { mount } from 'enzyme'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'
import Component, { VideoElements } from './index'
import toJson from 'enzyme-to-json'
Enzyme.configure({ adapter: new Adapter() })

const item = {
  title: 'Test title',
  channelTitle: 'Channel Title',
  publishTime: '2020-04-21T13:45:02Z',
  thumbnails: {
    default: {
      url: 'https://i.ytimg.com/vi/zOjov-2OZ0E/default.jpg'
    },
    medium: {
      url: 'https://i.ytimg.com/vi/zOjov-2OZ0E/mqdefault.jpg'
    },
    high: {
      url: 'https://i.ytimg.com/vi/zOjov-2OZ0E/hqdefault.jpg'
    }
  }
}

it('renders', () => {
  const tree = mount(<Component data={item} />)
  expect(toJson(tree)).toMatchSnapshot()
})

it('renders hover', async () => {
  const tree = mount(<Component data={item} />)
  expect(toJson(tree)).toMatchSnapshot()
  const videoElement = tree.find(VideoElements)
  videoElement.simulate('mouseEnter')
  expect(toJson(tree)).toMatchSnapshot()

  const videoElements = tree.find(VideoElements)
  const modalElement = tree.find('View.modal')
  // 2 video elements 1 modal found on hover
  expect(videoElements.length).toBe(2)
  expect(modalElement.length).toBe(1)
  expect(toJson(tree)).toMatchSnapshot()

  modalElement.at(0).simulate('mouseLeave')
  expect(toJson(tree)).toMatchSnapshot()
})
