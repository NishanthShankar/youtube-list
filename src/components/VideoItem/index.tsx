import { useState } from 'react'
import { View } from 'components/View'
import TooltipView from 'components/TooltipView'
import classes from './index.module.css'
import styles from './index.styles'
import { IVideoItemProps } from 'types'

export const VideoElements = (props: IVideoItemProps): JSX.Element => {
  return (
    <View style={styles.elementCtn} onMouseEnter={props.onMouseEnter}>
      <View style={styles.imageCtn}>
        <img
          style={{ width: '100%', aspectRatio: '1.8', objectFit: 'cover' }}
          src={props?.data?.thumbnails?.medium?.url}
        />
        {!(props.overlay ?? false) ? null : (
          <View style={styles.overlayTextCtn}>
            <span style={styles.overlayText}>
              Keep hovering to expand playing
            </span>
          </View>
        )}
      </View>
      <View style={styles.infoContainer}>
        <span style={styles.titleText}>{props?.data?.title}</span>
        <TooltipView tooltip={props?.data?.channelTitle}>
          <span style={styles.subText}>{props?.data?.channelTitle}</span>
        </TooltipView>
        {props?.data?.publishTime == null ||
        props?.data?.publishTime === '' ? null : (
          <span style={styles.subText}>
            {new Date(props?.data?.publishTime).toLocaleDateString()}
          </span>
        )}
      </View>
      {!(props.expanded ?? false) ? null : (
        <View center style={styles.watchLaterBtn}>
          Watch Later
        </View>
      )}
    </View>
  )
}

export default function VideoItem (props: IVideoItemProps): JSX.Element {
  const [hovering, setHovering] = useState(false)
  const [expanded, setExpanded] = useState(false)
  return (
    <View style={styles.container}>
      <VideoElements {...props} onMouseEnter={() => setHovering(true)} />
      {!hovering ? null : (
        <View
          className={`modal ${classes.animate}`}
          onMouseLeave={() => {
            setExpanded(false)
            setTimeout(() => setHovering(false), 300)
          }}
          onMouseEnter={() => setTimeout(() => setExpanded(true), 1000)}
          style={styles.modalCtn(expanded)}
        >
          <VideoElements {...props} overlay={!expanded} expanded={expanded} />
        </View>
      )}
    </View>
  )
}
