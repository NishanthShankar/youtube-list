import React, { useState } from 'react'
import { View } from 'components/View'

const styles = {
  tooltipContainer: {
    backgroundColor: '#6a6a6aEE',
    padding: '4px 10px',
    position: 'absolute',
    borderRadius: 4,
    top: -24,
    left: 0
  },
  tooltip: { color: 'white', fontSize: 12 }
}

export interface ITooltipViewProps {
  style?: object
  tooltip?: string
  children?: React.ReactNode
}

export default function TooltipView (
  props: ITooltipViewProps
): JSX.Element | null {
  const [hovering, setHovering] = useState(false)
  const styleProps = props.style != null || {}
  return (
    <View
      style={{ ...styleProps, position: 'relative' }}
      onMouseEnter={() => setHovering(true)}
      onMouseLeave={() => setHovering(false)}
    >
      {props.children}
      {hovering && props.tooltip != null && props.tooltip !== ''
        ? (
          <View style={styles.tooltipContainer}>
            <span style={styles.tooltip}>{props.tooltip}</span>
          </View>
          )
        : null}
    </View>
  )
}
