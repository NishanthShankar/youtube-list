import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'
import Component from './index'
import toJson from 'enzyme-to-json'

Enzyme.configure({ adapter: new Adapter() })

it('renders without children', () => {
  const tree = mount(<Component tooltip='Test' />)
  expect(toJson(tree)).toMatchSnapshot()
})

it('renders null with hide props', () => {
  const tree = mount(
    <Component tooltip='Test'>
      <span>Children</span>
    </Component>
  )
  expect(toJson(tree)).toMatchSnapshot()
})

it('renders tooltip', () => {
  const tree = mount(
    <Component tooltip='Test'>
      <span>Children</span>
    </Component>
  )
  expect(toJson(tree)).toMatchSnapshot()
  tree.simulate('mouseEnter')
  expect(toJson(tree)).toMatchSnapshot()
  tree.simulate('mouseLeave')
  expect(toJson(tree)).toMatchSnapshot()
})
// console.log(tree.debug({ verbose: true }))
