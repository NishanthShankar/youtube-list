import { useState } from 'react'
import SearchBar from 'components/SearchBar'
import ResultsView from './ResultsView'

export default function HomeView (): JSX.Element {
  const [query, setQuery] = useState('Programming')

  return (
    <>
      <SearchBar query={query} onSearch={setQuery} />
      <ResultsView query={query} />
    </>
  )
}
