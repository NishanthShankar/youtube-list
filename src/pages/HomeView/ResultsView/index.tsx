import { useState, useEffect } from 'react'
import { View, RView } from 'components/View'
import VideoItem from 'components/VideoItem'
import AutoFetcher from 'components/AutoFetcher'
import Spinner from 'components/Spinner'
import { useSearch } from 'hooks/'
import { IPageItemProps } from 'types'

export default function ResultsView (props: { query?: string }): JSX.Element {
  const [errorContent, setErrorContent] = useState()
  const {
    data,
    error,
    fetchNextPage,
    hasNextPage,
    isFetchingNextPage,
    status
  } = useSearch(props.query ?? '')

  useEffect(() => {
    let errorContent = null
    if (error != null && error !== '') errorContent = error?.message ?? 'Error'
    if (errorContent == null && status === 'loading') errorContent = <Spinner />
    const dataError = data?.pages?.[0].error ?? false
    if (errorContent == null && Boolean(dataError)) errorContent = 'Error'
    setErrorContent(errorContent)
  }, [error, status, data?.pages?.[0].error])
  // Check if any error or loading content has to be shown

  if (errorContent ?? false) {
    return (
      <View flex center>
        {errorContent}
      </View>
    )
  }
  const fetchingNextPage = Boolean(isFetchingNextPage ?? false)
  const nextPagePreset = Boolean(hasNextPage ?? false)

  // Render data
  return (
    <View>
      <RView style={{ flexWrap: 'wrap' }}>
        {data?.pages?.map((page: any) =>
          page.items.map((item: IPageItemProps) => (
            <VideoItem key={item?.id?.videoId} data={item.snippet} />
          ))
        )}
        {/* Show loader next to last known result */}
        <View style={{ alignSelf: 'center' }}>
          {fetchingNextPage
            ? (
              <Spinner />
              )
            : nextPagePreset
              ? (
                <AutoFetcher
                  hide={!fetchingNextPage && !nextPagePreset}
                  apiCall={fetchNextPage}
                />
                )
              : (
                  'Nothing more to load'
                )}
        </View>
      </RView>
    </View>
  )
}
