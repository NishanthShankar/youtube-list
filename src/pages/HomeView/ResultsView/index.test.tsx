// src/components/ProductDetails.test.tsx
import React from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import { useSearch } from 'hooks/'
import ResultsView from './index'
import { QueryClient, QueryClientProvider } from 'react-query'
import dummy from './dummy.json'

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false
    }
  }
})
// Solves TypeScript Errors
const mockedUseSearch = useSearch as jest.Mock<any>

// Mock the module
jest.mock('../../../hooks/')

const Element = (q: string): JSX.Element => (
  <QueryClientProvider client={queryClient}>
    <ResultsView query={q} />
  </QueryClientProvider>
)

describe('<ResultsView />', () => {
  beforeEach(() => {
    mockedUseSearch.mockImplementation(() => ({ isLoading: true }))
  })
  afterEach(() => {
    jest.clearAllMocks()
  })

  it('Renders without crashing', () => {
    render(
      <QueryClientProvider client={queryClient}>
        <ResultsView />
      </QueryClientProvider>
    )
  })

  it('Fetches the correct ID', () => {
    const { rerender } = render(Element('Progra'))

    // Fetches a default product when `productId` isn't specified (id="1")
    expect(useSearch).toHaveBeenCalledWith('Progra')

    rerender(Element('2'))

    expect(useSearch).toHaveBeenCalledWith('2')
  })

  it('Displays loading indicator', () => {
    const { getByText } = render(Element('Programming'))

    expect(getByText('Nothing more to load')).toBeVisible()
  })

  it('Displays error message', () => {
    mockedUseSearch.mockImplementation(() => ({
      isLoading: false,
      isError: true,
      error: { message: 'Unable to fetch the product data' }
    }))
    const { getByText, queryByText } = render(Element('22'))

    expect(queryByText(/fetching data/i)).toBeFalsy() // We don't want the loading flag to be displayed
    getByText(/unable to fetch the product data/i)
  })

  it('Displays data', () => {
    mockedUseSearch.mockImplementation(() => ({
      isLoading: false,
      data: { pages: [dummy] }
    }))

    const { getByText, queryByText } = render(Element('test'))

    expect(queryByText(/fetching data/i)).toBeFalsy()
    const data = dummy.items[0].snippet
    getByText(data.title)
    expect(render(Element('test'))).toMatchSnapshot()
    // getByText(data.channelTitle)
  })
})
