export interface SnippetProps {
  title: string
  channelTitle: string
  publishTime: string
  thumbnails: {
    default: {
      url: string
    }
    medium: {
      url: string
    }
    high: {
      url: string
    }
  }
}

export interface IVideoItemProps {
  onMouseEnter?: () => void
  overlay?: boolean
  expanded?: boolean
  data: SnippetProps
}

export interface IPageItemProps {
  id: {
    videoId: string
  }
  snippet: SnippetProps
}
