import { View } from 'components/View'
import HomeView from 'pages/HomeView'

// Add router in this level if need
function App (): JSX.Element {
  return (
    <View flex style={{ alignItems: 'center' }}>
      <View style={{ width: '100%', maxWidth: 1200 }}>
        <HomeView />
      </View>
    </View>
  )
}

export default App
