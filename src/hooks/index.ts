import { useInfiniteQuery } from 'react-query'

// Mock api const api = 'https://en9slatujs4zori.m.pipedream.net/?test=1'
const G_KEY = 'AIzaSyALeDOcqLyxVSRQxMRMhZMRc7WDv4cgDfk'
const api = `https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&maxResults=50&key=${G_KEY}`

const fetchSearch = (q: string) => async ({ pageParam = '' }): Promise<any> => {
  const res = await fetch(`${api}&pageToken=${pageParam}&q=${q}`)
  return await res.json()
}

export const useSearch = (q: string): any => {
  return useInfiniteQuery<any, Error>(['search', q], fetchSearch(q), {
    retry: false,
    getNextPageParam: (lastPage, pages) => lastPage.nextPageToken
  })
}
